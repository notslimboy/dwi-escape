﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// This class will handle game condition
/// </summary>
public class GameManager : MonoBehaviour
{
    private void Update()
    {
        CheckScore(); // check the score 
    }

    private void CheckScore()
    {
        if (Data.Score < 0) // load gameover scene when score < 0 
        {
            SceneManager.LoadScene("GameOver"); //load the scene 
        }
    }
}
