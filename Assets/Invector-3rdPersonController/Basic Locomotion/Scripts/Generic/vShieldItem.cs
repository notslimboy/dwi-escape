using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Invector.vCharacterController.TopDownShooter
{
    public class vShieldItem : MonoBehaviour
    {
        public string tagFilter = "Player";

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals(tagFilter))
            {
                // access the basic character information
                var tdController = other.GetComponent<vTopDownController>();
                if (tdController != null)
                {
                    tdController.Shield();
                    gameObject.SetActive(false);
                }
            }
        }
    }
}
